#!/bin/bash

# TODO: use `pwd`
rm /home/bohdan/.bashrc
ln -s /home/bohdan/.config/home-config/.bashrc /home/bohdan/.bashrc

ln -s /home/bohdan/.config/home-config/.config/fontconfig /home/bohdan/.config/fontconfig
ln -s /home/bohdan/.config/home-config/.fonts /home/bohdan/.fonts

rm /home/bohdan/.gitconfig
ln -s /home/bohdan/.config/home-config/.gitconfig /home/bohdan/.gitconfig

rm $VIRTUALENVWRAPPER_HOOK_DIR/postactivate
ln -s /home/bohdan/.config/home-config/virtualenvwrapper/postactivate $VIRTUALENVWRAPPER_HOOK_DIR/postactivate

fc-cache -f -v

echo "Now run source /home/bohdan/.bashrc"
