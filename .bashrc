export WORKON_HOME=~/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh

export PATH=$PATH:~/.composer/vendor/bin
export PATH=$PATH:~/.config/composer/vendor/bin
export PATH=$PATH:~/ShareD/Dotfiles/bin
export PATH=$PATH:~/ShareD/Dev/Tools/go_appengine
export GOPATH=~/ShareD/Dev/Go

txtblk='\e[0;30m' # Black - Regular
txtred='\e[0;31m' # Red
txtgrn='\e[0;32m' # Green
txtylw='\e[0;33m' # Yellow
txtblu='\e[0;34m' # Blue
txtpur='\e[0;35m' # Purple
txtcyn='\e[0;36m' # Cyan
txtwht='\e[0;37m' # White
bldblk='\e[1;30m' # Black - Bold
bldred='\e[1;31m' # Red
bldgrn='\e[1;32m' # Green
bldylw='\e[1;33m' # Yellow
bldblu='\e[1;34m' # Blue
bldpur='\e[1;35m' # Purple
bldcyn='\e[1;36m' # Cyan
bldwht='\e[1;37m' # White
unkblk='\e[4;30m' # Black - Underline
undred='\e[4;31m' # Red
undgrn='\e[4;32m' # Green
undylw='\e[4;33m' # Yellow
undblu='\e[4;34m' # Blue
undpur='\e[4;35m' # Purple
undcyn='\e[4;36m' # Cyan
undwht='\e[4;37m' # White
bakblk='\e[40m'   # Black - Background
bakred='\e[41m'   # Red
bakgrn='\e[42m'   # Green
bakylw='\e[43m'   # Yellow
bakblu='\e[44m'   # Blue
bakpur='\e[45m'   # Purple
bakcyn='\e[46m'   # Cyan
bakwht='\e[47m'   # White
txtrst='\e[0m'    # Text Reset


custom_git_ps1 () 
{ 
    local branch="$(git symbolic-ref HEAD 2>/dev/null)";
    if [ -n "$branch" ]; then
        printf "  %s" "${branch##refs/heads/}";
    else
        local detached="$(git rev-parse --short HEAD 2>/dev/null)";
        if [ -n "$detached" ]; then
            printf "  $txtred%s" "$detached";
        fi
    fi
}

workon_ps1 ()
{
    local b=`basename $VIRTUAL_ENV 2>/dev/null`;
    if [ -n "$b" ]; then
        printf " \e[0;34mⓔ %s" "$b";
    fi
}

export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[01;34m\] \w\[\033[01;32m\]$(custom_git_ps1)\[\033[01;34m\]$(workon_ps1)\n\e[0;37m└─ \@ ── \033[01;34m\$\[\033[00m\] '
